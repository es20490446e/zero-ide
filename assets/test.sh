#! /bin/bash

: '
=== PURPOSE ===

This program builds the software into the filesystem,
runs it, and then removes it. Ideal for testing.


=== HOW TO RUN ===

For executing it, in the application "Terminal" type:
"/pathToThisFolder/test.sh"


=== LEGALESE ===

Original code by Alberto Salvia Novella (es20490446e.wordpress.com)
Under the latest GNU Affero License
https://gitlab.com/es20490446e/zero-ide
'


here="$(realpath "$(dirname "${0}")")"
project="$(cd "${here}"; echo "${PWD##*/}")"
BINARY="/usr/bin/${project}"


mainFunction () {
	checkIfInstalled
	
	if [[ -f "${here}/build.sh" ]]; then
		local time; time="$(build)"
	fi
	
	"${here}/install-uninstall.sh" > /dev/null
	"${BINARY}"
	
	if [[ -f "${here}/build.sh" ]]; then
		printf "\nBuilt in: %s\n\n" "${time}"
	fi
		
	"${here}/install-uninstall.sh" > /dev/null
}


build () {
	local TIMEFORMAT="%3lR"
	# shellcheck disable=SC2005
	echo "$(time ("${here}/build.sh" >/dev/null 2>&1) 2>&1)"
}


checkIfInstalled () {
	if [[ -f "${BINARY}" ]]; then
		if [[ -d "/etc/install-uninstall/${project}" ]]; then
			"${here}/install-uninstall.sh" > /dev/null
		else
			echo "Another version of ${project} is already installed" >&2
			echo "Uninstall its package and run this program again"
			exit 1
		fi
	fi
}


configurePermissions () {
	sudo -v
	refreshPermissions "$$" &
}


prepareEnvironment () {
	set -e
	configurePermissions
}


refreshPermissions () {
	local pid="${1}"

	while kill -0 "${pid}" 2> /dev/null; do
		sudo -v
		sleep 10
	done
}


prepareEnvironment
mainFunction
