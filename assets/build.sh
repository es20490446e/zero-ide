#! /bin/bash

: '
=== LEGALESE ===

Original code by Alberto Salvia Novella (es20490446e.wordpress.com)
Under the latest GNU Affero License
https://gitlab.com/es20490446e/zero-ide
'

project="$(realpath "$(dirname "${0}")")"
builder="${here}/_BUILDER"


mainFunction () {
	mkdir --parents "${project}/${builder}"
	cd "${project}/${builder}"
	
	so "build: nil" echo # CUSTOMIZE THIS TO BUILD INTO "_ROOT"
}


so () {
	local tag="${1}"
	local commands="${*:2}"
	
	if ! error="$(eval "${commands}" 2>&1 >"/dev/null")" ; then
		if [ "${error}" == "" ] ; then
			error="Command failed: ${commands}"
		fi

		echo "${tag}: ${error}" >&2
		exit 1
	fi
}


set -e
mainFunction
